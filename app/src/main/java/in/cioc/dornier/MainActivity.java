package in.cioc.dornier;


import com.loopj.android.http.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {


    int ACCESS_WIFI_STATE_PERMISSION = 0;
    ProgressDialog progress;
    ImageView deviceIcon;


    public void connectToDevice(){
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());


        String[] words= ip.split("\\.");





        Log.d("Comparing " , "message");
        for (int i = 100; i < 254; i++) {

            Log.d("Comparing ", String.format("http://%s.%s.%s.%s/info.json", words[0] , words[1] , words[2] , i   ));


            AsyncHttpClient client = new AsyncHttpClient();
            client.get( String.format("http://%s.%s.%s.%s/info.json", words[0] , words[1] , words[2] , i   ), new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    // called when response HTTP status is "200 OK"




                    new Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progress.dismiss();


                                    TextView deviceID = (TextView) findViewById(R.id.deviceID);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        deviceIcon.setImageDrawable(   getDrawable(R.drawable.aries));
                                    }

                                    try {
                                        deviceID.setText(response.getString("deviceID"));
                                        TextView ipView = findViewById(R.id.ipAddress);
                                        ipView.setText(response.getString("ip"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            },
                            2000);


                }

            });




        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {


                    connectToDevice();


                }
                break;

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deviceIcon = findViewById(R.id.devicePic);



        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Please wait , searching for devices");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();



        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_WIFI_STATE}, ACCESS_WIFI_STATE_PERMISSION);
        } else {
            //TODO
            connectToDevice();
        }




    }
}
